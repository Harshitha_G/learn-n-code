#include <iostream>
#define QueueSize 99856
using namespace std;
class QueueOfSpiders
{
	private:
		int items[QueueSize], front, rear;
		
	public:
		QueueOfSpiders()
		{
			front = -1;
			rear = -1;
		}
		
		void EnQueue (int element)
		{
			if(!((rear==QueueSize-1 && front==0)||rear==front-1))
			{
				if(front==-1)
				{
					rear = 0;
					front = 0;
					items[rear] = element;
				}
				else if(rear==QueueSize-1 && front!=0)
				{
					rear = 0;
					items[rear] = element;
				}
				else
				{
					rear++;
					items[rear] = element;
				}
			}
		}
		
		int DeQueue ()
		{
			int element;
			
			if(front!=-1)
			{
				element = items[front];
				if (front==rear)
				{
					rear = -1;
					front = -1;
				}
				else if(front==QueueSize-1)
					front = 0;
				else
					front++;
					
			    return element;
			}
			else
			{
				return -1;
			}
		}
}PowerOfSpider, InitialPositionOfSpider;

class BatchOfSpiders:public QueueOfSpiders
{
	private:
		int NumberOfSpidersDequeued;
		int SelectedPower[316], SelectedPosition[316];

	public:
		void DeQueueBatchOfSpiders(int BatchSize)
		{
			int DequeuedPower, DequeuedPosition;
			NumberOfSpidersDequeued = 0;
			
			for(int Spider=0; Spider<BatchSize; Spider++)
			{
				DequeuedPower = PowerOfSpider.DeQueue();
				DequeuedPosition = InitialPositionOfSpider.DeQueue();
				if(DequeuedPower!=-1)
				{
					SelectedPower[Spider] = DequeuedPower ;
					SelectedPosition[Spider] = DequeuedPosition;
					NumberOfSpidersDequeued++;
				}
			}
		}
		
		int FindLargestPowerSpiderPosition()
		{
			int LargestPower = SelectedPower[0];
			int LargestPowerPosition = 0;
			
			for(int Spider=1; Spider<NumberOfSpidersDequeued; Spider++)
			{
				if(LargestPower<SelectedPower[Spider] && LargestPower!=SelectedPower[Spider])
				{
					LargestPower = SelectedPower[Spider];
					LargestPowerPosition = Spider;
				}
			}
			
			SelectedPower[LargestPowerPosition] = -1;
			
			return SelectedPosition[LargestPowerPosition];
		}
		
		void DecrementPowerOfSelectedSpiders()
		{
			for(int Spider=0; Spider<NumberOfSpidersDequeued; Spider++)
			{
				if(SelectedPower[Spider]>0)
					SelectedPower[Spider]--;
			}
		}
		
		void ReQueueBatchOfSpiders()
		{
			for(int Spider=0; Spider<NumberOfSpidersDequeued; Spider++)
			{
				if(SelectedPower[Spider]>=0)
				{
					PowerOfSpider.EnQueue(SelectedPower[Spider]);
					InitialPositionOfSpider.EnQueue(SelectedPosition[Spider]);
				}
			}
		}	
};

bool NumberOfSpidersInRange(int NumberOfSpiders, int MinNumberOfSpiders, int MaxNumberOfSpiders)
{
	if(NumberOfSpiders>=MinNumberOfSpiders && MaxNumberOfSpiders>=NumberOfSpiders)
	return true;
}

bool BatchSizeInRange(int BatchSize, int MinBatchSize, int MaxBatchSize)
{
	if(BatchSize>=MinBatchSize && MaxBatchSize>=BatchSize)
	return true;
}

bool SpiderPowerInRange(int SpiderPower,int MinSpiderPower,int MaxSpiderPower)
{
	if(SpiderPower>=MinSpiderPower && MaxSpiderPower>=SpiderPower)
	return true;
}

void EnQueueSpiders(int NumberOfSpiders, int BatchSize)
{
	int SpiderPower;
	const int MinSpiderPower = 1;
	const int MaxSpiderPower = BatchSize;
	
	for(int Spider=1; Spider<=NumberOfSpiders; Spider++)
	{
		cin>>SpiderPower;
		if(SpiderPowerInRange(SpiderPower, MinSpiderPower, MaxSpiderPower))
		{
			PowerOfSpider.EnQueue(SpiderPower);
			InitialPositionOfSpider.EnQueue(Spider);
		}
	}
}

int main()
{
	int NumberOfSpiders, BatchSize, SpiderPower;
	BatchOfSpiders OneBatch;
	
	cin>>NumberOfSpiders>>BatchSize;

	int ArrayOfLargestPowerPositions[BatchSize];
	const int MinNumberOfSpiders = BatchSize;
	const int MaxNumberOfSpiders = (BatchSize*BatchSize);
	const int MinBatchSize = 1;
	const int MaxBatchSize = 316;

	if( NumberOfSpidersInRange(NumberOfSpiders, MinNumberOfSpiders, MaxNumberOfSpiders) && BatchSizeInRange(BatchSize, MinBatchSize, MaxBatchSize) )
	{
		EnQueueSpiders(NumberOfSpiders, BatchSize);
		for(int EachBatch=0; EachBatch<BatchSize; EachBatch++)
		{
			OneBatch.DeQueueBatchOfSpiders(BatchSize);
			ArrayOfLargestPowerPositions[EachBatch] = OneBatch.FindLargestPowerSpiderPosition();
			OneBatch.DecrementPowerOfSelectedSpiders();
			OneBatch.ReQueueBatchOfSpiders();
			cout<<ArrayOfLargestPowerPositions[EachBatch]<<' ';
		}
	}
}