#include<stdio.h>
#include<stdlib.h>

struct Node* new_node (int data);
struct Node* insert_new_node (struct Node* node, int data);
struct Node* create_binary_search_tree (int total_number_of_elements, int array_of_elements[], struct Node* root_node);
void read_array_elements (int total_number_of_elements, int array[]);
int height_of_tree (struct Node* node);

struct Node 
{
    int data;
    struct Node *left, *right;
};

int main ()
{
    struct Node* root_node = NULL;
    int total_number_of_elements, array[1000];

    scanf("%d",&total_number_of_elements);

    read_array_elements(total_number_of_elements, array);
    
    root_node = create_binary_search_tree (total_number_of_elements, array, root_node);
    
    printf("%d", height_of_tree(root_node));
    return 0;
}

void read_array_elements (int total_number_of_elements, int array[])
{
    for(int i=0;i<total_number_of_elements;i++)
    {
        scanf("%d",&array[i]);
    }
}

struct Node* create_binary_search_tree (int total_number_of_elements, int array_of_elements[], struct Node* root_node)
{
    for(int i=0; i<total_number_of_elements; i++)
    {
        root_node = insert_new_node(root_node, array_of_elements[i]);
    }
    return root_node;
}

struct Node* insert_new_node(struct Node* node, int data)
{
    
    if (node == NULL) 
        return new_node(data);
 
    if (data < node->data)
        node->left  = insert_new_node(node->left, data);
    else if (data > node->data)
        node->right = insert_new_node(node->right, data);   
 
    return node;
}

struct Node* new_node (int data)
{
    struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
    new_node->data = data;
    new_node->left = new_node->right = NULL;
  
    return new_node;
}

int height_of_tree (struct Node* node) 
{
    if (node == NULL) 
        return 0;
    else
    {
        int height_of_left_sub_tree = height_of_tree(node->left);
        int height_of_right_sub_tree = height_of_tree(node->right);
        if (height_of_left_sub_tree > height_of_right_sub_tree)
        {
            return(height_of_left_sub_tree+1);
        }
        else 
            return(height_of_right_sub_tree+1);
    }
} 
