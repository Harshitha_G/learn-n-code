#include <stdio.h>

int main()
{
    int test_cases;
    //previous and current player id
    int current_id=0,previous_id=0,num_passes=0,temp=0;
    char pass_type=NULL;
    //printf("enter the number of test cases\n");
    scanf(" %d ",&test_cases);
    while(test_cases--)
    {
        //print f("enter the total number of passes and the first player id\n");
        scanf(" %d %d ", &num_passes,&current_id);
        while(num_passes--)   
        {    
            scanf(" %c ", &pass_type);
            if(pass_type=='B')
            {
                temp=previous_id;
                previous_id=current_id;
                current_id=temp;
            }
            else
            {
                previous_id=current_id;
                scanf(" %d ",&current_id);
            }
        }
        printf("Player %d\n",current_id);
    }
}
