#include<stdio.h>
#include<stdbool.h>

struct facebook
{
    int popularity;
    struct facebook *link;
};

typedef struct facebook friend;

friend *HEAD=NULL, *first_friend, *temp_head=NULL;
int friend_count=0, deleted_friends=0;

void add_one_friend(int value, int number_of_friends)
{
    friend *new_friend;
    HEAD=TAIL;
    if(HEAD == NULL)
    {
        new_friend = (friend *)malloc(sizeof(friend));
        new_friend->popularity = value;
        new_friend->link = NULL;
        HEAD = new_friend;
        first_friend = HEAD;
        friend_count++;
    }
    else
    {
        new_friend = (friend *)malloc(sizeof(friend));
        new_friend->popularity = value;
        new_friend->link = NULL;
        HEAD->link = new_friend;
        HEAD = new_friend;
        friend_count++;
    }
    if(friend_count==number_of_friends)
    {
        HEAD = first_friend;
        temp_head = HEAD;
    }
}

void delete_one_friend(int number_of_friends){
    bool friend_deleted = false;
    friend *current , *previous, *next ;
    current = temp_head;
    next = temp_head->link;
    for(int friend=1; friend<number_of_friends; friend++)
    {
        if(((current->popularity) < (next->popularity)) && (temp_head->link != NULL))
        {
            if(current == HEAD)
            {
                HEAD = next;
                free(current);
                temp_head = next;
            }
            else 
            {
                previous->link = next;
                free(current);
                current = next;
                next = next->link;
            }
            friend_deleted = true;
            temp_head = previous;
            break;
        }
        else
        {
            previous = current;
            current = next;
            next = next->link;
        }
    }
    if(friend_deleted == false)
    {
        previous->link = NULL;
        temp_head = previous;
        free(current);
    }
}

void print_friends()
{
    while(HEAD!=0)
    {
        printf("%d ",HEAD->popularity );
        HEAD = HEAD->link;
    }
    printf("\n");
}

bool in_range(int min_value, int input_value, int max_value)
{
    return(input_value>=min_value && input_value<=max_value);
}

void delete_friends(int number_of_friends, int friends_to_remove)
{ 
    for(int friend=0; friend<friends_to_remove; friend++)
    {
        delete_one_friend(number_of_friends);   
        printf("%d\n", friend);
       
    }
}

void add_friends(int popularity, int number_of_friends)
{
    const int min_popularity = 0;
    const int max_popularity = 100;
    for(int friend=0; friend<number_of_friends; friend++)
    {
        scanf(" %d ",&popularity);
        if(in_range(min_popularity, popularity, max_popularity))
        {
            add_one_friend(popularity, number_of_friends);
        }
        else
        {
            scanf(" %d ",&popularity);
        }
    }
}

void read_inputs(int* number_of_friends, int* friends_to_delete)
{
    const int min_friends = 1;
    const int max_friends = 100000;
    const int min_friends_to_delete = 0;
    const int max_friends_to_delete = number_of_friends-1;
    scanf(" %d %d", number_of_friends, friends_to_delete);
}

int main()
{
    const int min_iterations = 1;
    const int max_iterations = 1000;
    
    int iterations, number_of_friends, friends_to_delete, popularity;

    scanf(" %d", &iterations);
    if(in_range(min_iterations, iterations, max_iterations))
    {
        for(int i=0; i<iterations; i++)
        {
            read_inputs(&number_of_friends, &friends_to_delete);
            add_friends(popularity, number_of_friends);
            delete_friends(number_of_friends, friends_to_delete);
            print_friends();
            friend_count=0;
            temp_head = NULL;
        }
    }
}

