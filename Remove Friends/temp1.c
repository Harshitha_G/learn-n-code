#include<stdio.h>
#include<stdbool.h>

struct facebook
{
    int popularity;
    struct facebook *link;
};

typedef struct facebook friend;

friend *HEAD=NULL, *temp_head=NULL, *first_friend;
int friend_count=0;

void add_one_friend(int value, int number_of_friends)
{
    friend *new_friend;
    if(HEAD == NULL)
    {
        new_friend = (friend *)malloc(sizeof(friend));
        new_friend->popularity = value;
        new_friend->link = NULL;
        HEAD = new_friend;
        first_friend = HEAD;
        friend_count++;
    }
    else
    {
        new_friend = (friend *)malloc(sizeof(friend));
        new_friend->popularity = value;
        new_friend->link = NULL;
        HEAD->link = new_friend;
        HEAD = new_friend;
        friend_count++;
    }
    if(friend_count==number_of_friends)
    {
        HEAD = first_friend;
        temp_head = first_friend;
        printf("temp_head=%d\n", temp_head->popularity);
    }
}

int delete_one_friend(int number_of_friends){
    bool friend_deleted = false;
    friend *current , *previous, *next ;
    current = HEAD;
    next = HEAD->link;
    int deleted_position;
    printf("beg_curr=%d beg_next=%d\n", current->popularity, next->popularity);
    for(int friend=1; friend<number_of_friends; friend++)
    {
        if(current->popularity<next->popularity)
        {
            if(current == HEAD)
            {
                HEAD = next;
                free(current);
                temp_head = HEAD;
                deleted_position = friend+1;
                printf(" temp=%d\n", temp_head->popularity);
            }
            else 
            {
                printf("enered else\n");
                previous->link = next;
                free(current);
                current = next;
                next = next->link;
                //temp_head->popularity = previous->popularity;
                //temp_head->link = previous->link; 
                deleted_position = friend;
                printf("temp=%d , current=%d , next=%d \n",next->popularity,current->popularity,next->popularity);
            }
            friend_deleted = true;
            //printf("\n %d=dp\n",deleted_position);
            break;
        }
        else
        {
            previous = current;
            current = next;
            next = next->link;
            printf("after if failed, curr=%d, next=%d , prev=%d\n", current->popularity, next->popularity, next->popularity);
        }
    }
    if(friend_deleted == false)
    {
        previous->link = current->link;
        printf("dleting last %d, pre=%d\n", current->popularity, next->popularity);
        free(current);
        //deleted_position = number_of_friends;
        
    }
    return deleted_position;
}

void print_friends()
{
    while(HEAD!=0)
    {
        printf("%d ",HEAD->popularity );
        HEAD = HEAD->link;
    }
    printf("\n");
}

bool in_range(int min_value, int input_value, int max_value)
{
    return(input_value>=min_value && input_value<=max_value);
}

void delete_friends(int number_of_friends, int friends_to_remove)
{
    int deleted_position;
    for(int friend=0; friend<friends_to_remove; friend++)
    {
        deleted_position = delete_one_friend(number_of_friends);
        //number_of_friends = number_of_friends - (deleted_position - 1);
        printf("num=%d , dp=%d\n", number_of_friends, deleted_position);
    }
}

void add_friends(int popularity, int number_of_friends)
{
    const int min_popularity = 0;
    const int max_popularity = 100;

    for(int friend=0; friend<number_of_friends; friend++)
    {
        scanf(" %d ",&popularity);
        if(in_range(min_popularity, popularity, max_popularity))
        {
            add_one_friend(popularity, number_of_friends);
        }
        else
        {
            scanf(" %d ",&popularity);
        }
    }
}

void read_inputs(int* number_of_friends, int* friends_to_delete)
{
    const int min_friends = 1;
    const int max_friends = 100000;
    const int min_friends_to_delete = 0;
    const int max_friends_to_delete = number_of_friends-1;

    scanf(" %d %d", number_of_friends, friends_to_delete);
}

int main()
{
    const int min_iterations = 1;
    const int max_iterations = 1000;
    
    int iterations, number_of_friends, friends_to_delete, popularity;

    scanf(" %d", &iterations);
    if(in_range(min_iterations, iterations, max_iterations))
    {
        for(int i=0; i<iterations; i++)
        {
            read_inputs(&number_of_friends, &friends_to_delete);
            add_friends(popularity, number_of_friends);
            delete_friends(number_of_friends, friends_to_delete);
            print_friends();
            friend_count=0;
            temp_head=NULL;
        }
    }
}

