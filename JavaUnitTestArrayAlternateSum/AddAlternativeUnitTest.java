import static org.junit.Assert.*;
import org.junit.Test;


public class AddAlternativeUnitTest implements AlternateSum {

	@Test
	public void CountOfElemetsTest(int[] matrix_array) throws Exception
    {
        //Arrange
		AlternateNumbersSum calculateWithMatrix = new AlternateNumbersSum();
        int expected = 9;

        //Act
        int numberofElements = calculateWithMatrix.CountOfElemets(matrix_array);

        //Assert
        assertEquals(expected, numberofElements);
    }
	
	@Test
	public void IsValidInputTest(int num) throws Exception
    {
        //Arrange
		AlternateNumbersSum calculateWithMatrix = new AlternateNumbersSum();
        Boolean expected = true;

        //Act
        Boolean isValid = calculateWithMatrix.IsValidInput(6);

        //Assert
        assertEquals(expected, isValid);
    }

    @Test
    public void AddAlternateArrayElementsTest(int startIndex, int[] matrix_array) throws Exception
    {
        //Arrange
        AlternateNumbersSum calculateWithMatrix = new AlternateNumbersSum();
        int expected = 20;

        //Act
        int sumOffAlternateArrayElements = calculateWithMatrix.AddAlternateArrayElements(startIndex, matrix_array);

        //Assert
        assertEquals(expected, SumOfAlternateArrayElements);
    }
}
