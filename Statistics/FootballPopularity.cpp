
#include<iostream>
#include<map>
using namespace std;

map<string,string>name_and_sport;
map<string,int>sport_and_popularity;
map<string,int>:: iterator index;

int add_name_and_sport(int no_of_people)
{
	string name,sport,popular_sport;
	int count = 1, no_of_sports = 0;

	while(no_of_people--)
	{
		cin >> name >> sport;

		if(name_and_sport.count(name) == 0)
		{
			name_and_sport.insert(pair<string,string>(name,sport));

			if(sport_and_popularity.count(sport) == 0)
			{
				sport_and_popularity.insert(pair<string,int>(sport,count));
				no_of_sports++;
			}
			else 
			{
				(sport_and_popularity.find(sport)->second)++;
			}
		}
	}
	return no_of_sports;
}

string find_popular_sport(int no_of_sports)
{
	string popular_sport=sport_and_popularity.begin()->first;
	int like_count = sport_and_popularity.begin()->second;

	for(index=sport_and_popularity.begin();  index!=sport_and_popularity.end(); index++)
	{
		if(index->second > like_count) 
		{
			like_count = index->second;
			popular_sport = index->first;
		}
	}
	return popular_sport;
}

int print_football_count()
{
	if(sport_and_popularity.count("football") > 0)
	{
		cout << sport_and_popularity.find("football")->second;
	}
	else
	{
		cout << 0;
	}
}

int main()
{
string popular_sport;
int no_of_people, no_of_sports;
int  like_count = -1;

cin>>no_of_people;

no_of_sports = add_name_and_sport(no_of_people);
popular_sport= find_popular_sport(no_of_sports);
cout << popular_sport << endl;
print_football_count();
}
