#include <stdio.h>
#include <stdbool.h>

int current_id,previous_id,num_passes;
int test_cases;
char pass_type=NULL;

//minimum and maximum number of tests(games),passes and players
const int min_test_cases = 1;
const int max_test_cases = 100;
const int min_num_passes = 1;
const int max_num_passes = 100000;
const int min_current_id = 1;
const int max_current_id = 1000000;
const int null = 0;

//function definitions
bool in_range(int input_value, int min_value, int max_value);
void forward_pass();
void backward_pass();
void playing();
void read_test_cases();
void read_num_passes_and_current_id();

int main()
{
    read_test_cases();
	while(test_cases != null)
    {
		read_num_passes_and_current_id();
		playing();
		test_cases--;
		printf("Player %d\n",current_id);
	}
}

//read number of test cases until it's valid
void read_test_cases()
{
	scanf(" %d ",&test_cases);
	while(!in_range(test_cases, min_test_cases, max_test_cases))
	{
		scanf(" %d ",&test_cases);
	}
}

//read the number of passes and player id until they are valid
void read_num_passes_and_current_id()
{
    scanf(" %d %d ", &num_passes, &current_id);
	while(!in_range(num_passes, min_num_passes, max_num_passes) || !in_range(current_id, min_current_id, max_current_id))
	{
		scanf(" %d %d ", &num_passes,&current_id);
	}
}

//to check if input values are in given range
bool in_range(int input_value, int min_value, int max_value)
{
	return(input_value >= min_value && input_value <= max_value);		
}

//one complete test or game
void playing()
{	
	const char backward_pass_type = 'B';
	const char forward_pass_type = 'P';
	while(num_passes != null)  
	{    
		scanf(" %c ", &pass_type);
		if(pass_type == forward_pass_type)
		{
			forward_pass();
			num_passes--;
		}
		else if(pass_type == backward_pass_type)
		{
			backward_pass();
			num_passes--;
		}
	}
}

//pass the ball to next player
void forward_pass()
{	
	bool new_current_id_valid=false;
	int new_current_id;
	previous_id = current_id;
	scanf(" %d ",&current_id);
	while(!in_range(current_id, min_current_id, max_current_id))
	{
		scanf(" %d ",&current_id);
	}
}

//pass the ball back to the previous player
void backward_pass()
{
	int temp;
	temp = previous_id;
	previous_id = current_id;
	current_id = temp;
}




